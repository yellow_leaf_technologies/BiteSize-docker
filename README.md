#### Installing docker for BiteSize apps
##### Installation requirements
    docker
    docker-engine
For docker installation instruction follow this [link](https://docs.docker.com/install/).

Create new directory called BiteSize and change current directory to newly created.
##### Cloning projects
Git clone `admin` and `api` projects to current directory.

Git clone current docker project to BiteSize directory.

##### Copy docker-compose.yml

Copy docker-compose.yml and .dockerignore files from docker directory to the root of BiteSize directory.

File structure of BiteSize should be:
```
    ...
    BiteSize
    |    admin
    |    api
    |    docker
    |    |    admin
    |    |    |    Dockerfile
    |    |    api
    |    |    |   Dockerfile
    |    |    db
    |    |    |    Dockerfile
    |    |   wait-for-it.sh
    |    docker-compose.yml
    |    .dockerignore
    ...
```
Next run the following command 
```
docker-compose up 
```
